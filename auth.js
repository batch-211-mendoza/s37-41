// auth.js
/*
	- auth.js is our own module which will contain methods to help authorize or restrict users from accessing certain features in our app
*/

const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

// JSON Web Tokens
/*
	- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of the server

	JSONWebToken Structure
	It has 3 parts:
	1. header
		- tells us about the algorithm
	2. payload
		- the data
	3. signature
		- the combination of the header, payload and secret

*/

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	return jwt.sign(data, secret, {});
};

// Token Verification
/*
	Receive the gift (JSONWebtoken) and open the lock to verify if the sender is legitimate and the gift was not tampered with.
*/

module.exports.verify = (request, response, next) => {
	// The token is retrieved from the request header
	// This can be provided in postman under:
		// Authotization > Bearer Token
	let token = request.headers.authorization;

	if (typeof token !== "undefined"){
		token = token.slice(7, token.length); // The "slice" method takes only the token from the information send via the request header
		// Bearer ylfojfddpofk

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return response.send({auth: "Failed to verify."});
			} else{
				// "next()" method allows the app to proceed with the next middleware function/callback function
				next();
			}
		});
	} else{
		return response.send({auth: "Failed. Token undefined."});
	}
}

// Token Decryption
/*
	Open the gift and get the content
*/

module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return null;
			} else{
				return jwt.decode(token, {complete: true}).payload;
			}
		});
	} else {
		return null;
	}
}