// Import packages
const bcrypt = require("bcrypt");

// Import authentication js
const auth = require("../auth");

// Import model
const Course = require("../models/Course");

// module.exports.addCourse = (requestBody) => {
// 	let newCourse = new Course({
// 			name: requestBody.course.name,
// 			description: requestBody.course.description,
// 			price: requestBody.course.price,
// 	});	

// 	if(requestBody.isAdmin !== true){
// 		return false;
// 	} else{
// 		return newCourse.save().then((course, error) => {
// 			if(error) {
// 				return false;
// 			} else {
// 				return true;
// 			};
// 		});
// 	}
// }

module.exports.addCourse = (data) => {
	console.log(data);
	if(data.isAdmin){
		let newCourse = new Course({
				name: data.course.name,
				description: data.course.description,
				price: data.course.price,
		});
		
		return newCourse.save().then((course, error) => {
			if(error) {
				return false;
			} else {
				return true;
			};
		});
	} else{
		return false;
	}
}

// Retrieve all courses
/*
	1. Retrieve all the courses from the database

	Syntax:
		Model.find({});
*/
module.exports.getAllCourses = () => {
	return Course.find({}).then((result) => {
		return result;
	})
}

// Retrieve all Active Courses
/*
	1. Retrieve all the courses from the database with the property of "isActive" to true
*/
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then((result) => {
		return result;
	});
}

// Retrieving a specific course
/*
	1. Retrieve the course that matches the course ID provided from the URL
*/
module.exports.getCourse = (requestParams) => {
	return Course.findById(requestParams.courseId).then((result) => {
		return result;
	});
}

// Updating a course
/*
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
module.exports.updateCourse = (requestParams, requestBody) => {
	let updatedCourse = {
		name: requestBody.name,
		description: requestBody.description,
		price: requestBody.price
	};
	return Course.findByIdAndUpdate(requestParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		} else{
			return true;
		}
	});
}

// ----- ACTIVITY S40 ----- //
// My Solution
// module.exports.archiveCourse = (requestParams, data) => {

// 	if(data.isAdmin !== true){
// 		return false;
// 	} else {
// 		let archivedCourse = {
// 			isActive: false
// 		};
// 		return Course.findByIdAndUpdate(requestParams.courseId, archivedCourse).then((course, error) => {
// 			if(error){
// 				return false;
// 			} else{
// 				return archivedCourse;
// 			}
// 		});
// 	}
// }

// S40 Activity Instructor's Solution
module.exports.archiveCourse = (requestParams, data) => {
	if(data.isAdmin){
		return Course.findByIdAndUpdate(requestParams, {isActive: false}).then((course, error) => {
			if(error){
				return false;
			} else{
				return true;
			}
		});	
	} else {
		return false;
	}
}

module.exports.activateCourse = (requestParams, data) => {
	if(data.isAdmin){
		return Course.findByIdAndUpdate(requestParams, {isActive: true}).then((course, error) => {
			if(error){
				return false;
			} else{
				return true;
			}
		});	
	} else {
		return false;
	}
}