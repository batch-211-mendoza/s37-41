// Import packages
// bcrypt is a package which allows us to hash our passwords to add a layer of security for our user's details
const bcrypt = require("bcrypt");

// Import authentication js
const auth = require("../auth");

// Import model
const User = require("../models/User");
const Course = require("../models/Course");



// Check if the email already exists
/*
	Steps:
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the FE application based on the result of the "find" method
*/

module.exports.checkEmailExists = (requestBody) => {
	return User.find({email: requestBody.email}). then((result) => {
		if(result.length > 0){
			return result;
		} else {
			return false;
		}
	});
}

// User Registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the info from the requestBody
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (requestBody) => {
	let newUser = new User({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		password: bcrypt.hashSync(requestBody.password,10)
	});
	return newUser.save().then((user, error) => {
		if(error){
			return false;
		} else{
			return true;
		}
	});
}

// User Authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password in the login form with the password stores in the database
	3. Generate/return a JSON Web token if the user is successfully logged in, and return false if not
*/

module.exports.loginUser = (requestBody) => {
	return User.findOne({email: requestBody.email}).then((result) => {
		if(result == null){
			return false;
		} else{
			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			} else{
				return false;
			}
		}
	})
}

// ----- ACTIVITY ----- //
// My Solution

// module.exports.getProfile = (requestBody) => {
// 	return User.findOne({_id: requestBody._id}).then((result, error) => {
// 		if(error){
// 			console.log(error);
// 			return false;
// 		} else {
// 			return {
// 				_id: result._id,
// 				firstName: result.firstName,
// 				lastName: result.lastName,
// 				email: result.email,
// 				password: "",
// 				isAdmin: result.isAdmin,
// 				mobileNo: result.mobileNo,
// 				enrollments: result.enrollments,
// 				__v: result.__v	
// 			};
// 		}
// 	});
// }

// Instructor's Solution #1 for the Activity
// Retrieve user details
/*
	Steps:
	1. Find the document in the db using the user's ID
	2. Reassign the password of the returned document to an empty string
	3. Return the result back to the front-end
*/

// module.exports.getProfile = (requestBody) => {
// 	return User.findById(requestBody.id).then(result => {
// 		result.password = "";
// 		return result;
// 	});
// }

// Instructor's Solution #2 for the Activity
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then((result) => {
		result.password = "";
		return result;
	});
}

// Enroll user to a class
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the courseId to the user's enrollment array
	3. Update the document in MongoDB Atlas
*/

/*
	1. First, find the user who is enrolling and update his enrollments subdocument array. We will push the courseId in the enrollments array.

	2. Second, find the course where we are enrolling and update its enrollees. We will push the userId in the enrollees array.

	*Since we will access 2 collections in one action, we will have to wait for the completion of the action instead of letting JavaScript continue line per line.

	To be able to wait for the result of a function, we use the "await" keyword.
	The "await" keyword allows us to wait for the function to finish and get a result before proceeding.
*/

module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then((user) => {

		user.enrollments.push({courseId:data.courseId});

		return user.save().then((user, error) => {
			if(error){
				return false;
			} else{
				return true;
			}
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then((course) => {

		course.enrollees.push({userId: data.userId});

		return course.save().then((course, error) => {
			if(error){
				return false;
			} else{
				return true;
			}
		});
	});

	if(isUserUpdated && isCourseUpdated){
		return true;
	} else{
		return false;
	}
}