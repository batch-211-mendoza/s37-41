// Import packages
const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");

// Initialize express.js
const app = express();

// Connect to database
mongoose.connect("mongodb+srv://admin123:admin123@project0.c7wlest.mongodb.net/s37-41?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
});

// Notification about MongoDB Atlas connection status
let db = mongoose.connection;
db.on("error", () => {console.error.bind(console, "Connection error")});
db.once("open", () => {console.log("Now connected to MongoDB Atlas.")});

// Allows all resources to access our backend application
app.use(cors());

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// For all user routes, the endpoint will start with /users
app.use("/users", userRoutes);

// For all course routes, the endpoint will start with /courses
app.use("/courses", courseRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
});