/*

App: Booking System API

Scenario:
	A course booking system application where a user can enroll into a course

Type: Course Booking System (Web App)

Description:
	- A course booking system application where a user can enroll into course
	- Allows an admin to do CRUD operations
	- Allows users to register into our database

Features:
	- User Registration
	- User Login (User Authentication)

	Customer/Authenticated Users:
	- View Courses (All active courses)
	- Enroll Course

	Admin Users:
	- Add Course
	- Update Course
	- Archive/Unarchive Course (Soft delete/reactivate the course)
	- View Courses (All courses active/inactive)
	- View/Manage User Accounts

	All Users (guests, customers, admin):
	- View Active Courses
*/

// Data Model for the Booking System

// Two-way Embedding

/*

user{
	id, //unique identifier for the document
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin,
	enrollment: [
		{	
			id, //document identifier
			courseId, //unique identifer
			courseName, //optional
			isPaid,
			dateEnrolled
		}
	]
}

course{
	id, //unique document identifier
	name,
	description,
	price,
	slots,
	isActive,
	createdOn,
	enrollees: [
		{
			id, // document identifier
			userId, //unique document 
			isPaid,
			dateEnrolled
		}
	]
}

*/

// With Referencing

/*

user{
	id, //unique identifier for the document
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin,
	enrollment: [
		{	
			id, //document identifier
			courseId, //unique identifer
			courseName, //optional
			isPaid,
			dateEnrolled
		}
	]
}

course{
	id, //unique document identifier
	name,
	description,
	price,
	slots,
	isActive,
	createdOn,
	enrollees: [
		{
			id, // document identifier
			userId, //unique document 
			isPaid,
			dateEnrolled
		}
	]
}

enrollment{
	id, //unique document identifier
	userId, //unique document identifier for user
	courseId, //unique document identifier for course
	courseName, //optional
	isPaid,
	dateEnrolled
}
*/

/*
	Mini-Activity
		1. Create a "models" folder and create a "Course.js" file to store the schema of our courses
		2. Put the necessary data types of our fields
*/