const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course name cannot be blank"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Please indicate course price"]
	},

	// slots: {
	// 	type: Number,
	// 	required: [true, "Please indicate slots available"]
	// },

	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date() // may be written as "Date.now"
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "userId is required"]
			},
			dateEnrolled: {
				type: Date,
				default: new Date() // may be written as "Date.now"
			}
			// isPaid: {
			// 	type: Boolean,
			// 	default: false
			// },
		}
	]
});

module.exports = mongoose.model("Course", courseSchema);