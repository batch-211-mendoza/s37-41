const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userControllers");

// Route for checking if the user's email already exists in the database
// Pass the "body" property of our request

router.post("/checkEmail", (request, response) => {
	userController.checkEmailExists(request.body).then((resultFromController) => {
		response.send(resultFromController)
	});
});

// Route for user registration
router.post("/register", (request, response) => {
	userController.registerUser(request.body).then((resultFromController) => {
		response.send(resultFromController)
	});
})

// Route for user authentication
router.post("/login", (request, response) => {
	userController.loginUser(request.body).then((resultFromController) => {
		response.send(resultFromController);
	});
});

// ----- ACTIVITY ----- //
// My Solution

// router.post("/details", (request, response) => {
// 	userController.getProfile(request.body).then(resultFromController => {
// 		response.send(resultFromController);
// 	});
// });

// Instructor's Solution #1 for the Activity
//Route for retrieving user details

// router.post("/details", (request, response) => {
// 	userController.getProfile(request.body).then(resultFromController => {
// 		response.send(resultFromController);
// 	});
// });

// Instructor's Solution #2 for the Activity

router.get("/details", auth.verify, (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	userController.getProfile({userId: userData.id}).then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route to enroll a user to a course
router.post("/enroll", auth.verify, (request, response) => {
	let data = {
		userId: auth.decode(request.headers.authorization).id,
		courseId: request.body.courseId
	}

	userController.enroll(data).then((resultFromController) => {
		response.send(resultFromController);
	});
});

module.exports = router;

