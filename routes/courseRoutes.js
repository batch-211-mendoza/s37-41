const express = require("express");
const router = express.Router();
const auth = require("../auth");

const courseController = require("../controllers/courseControllers");

// Route for creating a course
router.post("/", auth.verify, (request, response) => {
	const data = {
		course: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	};
	courseController.addCourse(data).then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for retrieving all the courses
router.get("/all", (request, response) => {
	courseController.getAllCourses().then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for retrieving all active courses
router.get("/", (request, response) => {
	courseController.getAllActive().then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for retrieving a specific course
router.get("/:courseId", (request, response) => {
	courseController.getCourse(request.params).then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for updating a course
router.put("/:courseId", auth.verify, (request, response) => {
	courseController.updateCourse(request.params, request.body).then((resultFromController) => {
		response.send(resultFromController);
	});
});

// ----- ACTIVITY S40 ----- //

router.put("/:courseId/archive", auth.verify, (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	courseController.archiveCourse(request.params, userData).then((resultFromController) => {
		response.send(resultFromController);
	});
});

module.exports = router;